var req = new XMLHttpRequest();
var url ='http://localhost/Ecommerce/api/detalles.php'+window.location.search;
req.open("GET",url,true);
var $lista = document.getElementById('producto');
function traerLista(){
    req.onreadystatechange = function(){
       if(req.status==200 && req.readyState==4){
            var respuesta = JSON.parse(req.responseText);
            console.log(respuesta.response);
            crearLista(respuesta.response);
       }
    };
    req.send(null);
};
function crearLista(res){
    
    var lista = '';
    var $mostrar = document.getElementById('producto');
    
    res.forEach(function(elemento, index){
        lista += ' <div class="containerp"><div class="cajadetalles"><div class="imagen"><img src="images/' + elemento.portada + '"></div></div>' 
                +'<div class="informacion"><h1>'+ elemento.nombre +'</h1>'
                +'<h3>Autor : '+ elemento.autor +'</h3>'
                +'<h3>Genero : '+ elemento.genero +'</h3>'
                +'<h3>Editorial : '+ elemento.editorial +'</h3>' 
                +'<h2>$'+ elemento.precio +'</h2></div>'
                +'<div class="botoncomprar" onclick="redireccion()"><button class="btn_comprar" id="comprar">Comprar</button></div>'
                +'<div class="cajasinopsis">'+elemento.sinopsis+'</div></div>';
                    
            });
    $mostrar.innerHTML = lista; 
}
window.onload = function(){
    traerLista();
};

function redireccion() {
    window.location="http://localhost:3000/formulario.html";
}
